package TestSort;

import junit.framework.TestCase;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.sort.CompararPeliculas;
import model.sort.ShellSort;
import model.vo.VOPelicula;

public class ShellSortTest extends TestCase{

	private ILista<VOPelicula> peliculasPrueba;
	
	/**
	 * Peliculas sin repeticion
	 */
	public void setupEscenario1(){
		peliculasPrueba = new ListaEncadenada<VOPelicula>();
		VOPelicula nueva1 = new VOPelicula();
		VOPelicula nueva2 = new VOPelicula();
		VOPelicula nueva3 = new VOPelicula();
		VOPelicula nueva4 = new VOPelicula();
		VOPelicula nueva5 = new VOPelicula();
		VOPelicula nueva6 = new VOPelicula();
		VOPelicula nueva7 = new VOPelicula();
		VOPelicula nueva8 = new VOPelicula();
		VOPelicula nueva9 = new VOPelicula();
		VOPelicula nueva10 = new VOPelicula();
		nueva1.setTitulo("Bill & Ted's Bogus Journey ");//
		nueva1.setAgnoPublicacion(1985);
		nueva1.setPromedioRatings(3.2);
		peliculasPrueba.agregarElementoFinal(nueva1);
		nueva2.setTitulo("Boy Who Could Fly, The ");//
		nueva2.setAgnoPublicacion(1976);
		nueva2.setPromedioRatings(4.2);
		peliculasPrueba.agregarElementoFinal(nueva2); 
		nueva3.setTitulo("Following Sean ");//
		nueva3.setAgnoPublicacion(1990);
		nueva3.setPromedioRatings(4.6);
		peliculasPrueba.agregarElementoFinal(nueva3);
		nueva4.setTitulo("How to Stuff a Wild Bikini ");//
		nueva4.setAgnoPublicacion(1982);
		nueva4.setPromedioRatings(2.8);
		peliculasPrueba.agregarElementoFinal(nueva4);
		nueva5.setTitulo("Pelle the Conqueror (Pelle erobreren) ");//
		nueva5.setAgnoPublicacion(1974);
		nueva5.setPromedioRatings(3.8);
		peliculasPrueba.agregarElementoFinal(nueva5);
		nueva6.setTitulo("Wanted ");
		nueva6.setAgnoPublicacion(2003);
		nueva6.setPromedioRatings(4.5);
		peliculasPrueba.agregarElementoFinal(nueva6);
		nueva7.setTitulo("Life After Tomorrow ");
		nueva7.setAgnoPublicacion(2006);
		nueva7.setPromedioRatings(3.1);
		peliculasPrueba.agregarElementoFinal(nueva7);
		nueva8.setTitulo("My Favorite Martian ");
		nueva8.setAgnoPublicacion(2000);
		nueva8.setPromedioRatings(2.9);
		peliculasPrueba.agregarElementoFinal(nueva8);
		nueva9.setTitulo("River Wild, The ");//
		nueva9.setAgnoPublicacion(1992);
		nueva9.setPromedioRatings(4.3);
		peliculasPrueba.agregarElementoFinal(nueva9);
		nueva10.setTitulo("Remains of the Day, The ");//
		nueva10.setAgnoPublicacion(1914);
		nueva10.setPromedioRatings(1.6);
		peliculasPrueba.agregarElementoFinal(nueva10);	
	}
	
	
	/**
	 * Peliculas con a�o de publicaci�n repetido
	 */
	public void setupEscenario2(){
		peliculasPrueba = new ListaEncadenada<VOPelicula>();
		VOPelicula nueva1 = new VOPelicula();
		VOPelicula nueva2 = new VOPelicula();
		VOPelicula nueva3 = new VOPelicula();
		VOPelicula nueva4 = new VOPelicula();
		VOPelicula nueva5 = new VOPelicula();
		VOPelicula nueva6 = new VOPelicula();
		VOPelicula nueva7 = new VOPelicula();
		VOPelicula nueva8 = new VOPelicula();
		VOPelicula nueva9 = new VOPelicula();
		VOPelicula nueva10 = new VOPelicula();
		nueva1.setTitulo("Bill & Ted's Bogus Journey ");
		nueva1.setAgnoPublicacion(1985);
		nueva1.setPromedioRatings(3.2);
		peliculasPrueba.agregarElementoFinal(nueva1);
		nueva2.setTitulo("Boy Who Could Fly, The ");
		nueva2.setAgnoPublicacion(1976);
		nueva2.setPromedioRatings(4.2);
		peliculasPrueba.agregarElementoFinal(nueva2);
		nueva3.setTitulo("Following Sean ");
		nueva3.setAgnoPublicacion(1990);
		nueva3.setPromedioRatings(4.6);
		peliculasPrueba.agregarElementoFinal(nueva3);
		nueva4.setTitulo("How to Stuff a Wild Bikini ");
		nueva4.setAgnoPublicacion(1982);
		nueva4.setPromedioRatings(2.8);
		peliculasPrueba.agregarElementoFinal(nueva4);
		nueva5.setTitulo("Pelle the Conqueror (Pelle erobreren) ");
		nueva5.setAgnoPublicacion(1985);
		nueva5.setPromedioRatings(3.8);
		peliculasPrueba.agregarElementoFinal(nueva5);
		nueva6.setTitulo("Wanted ");
		nueva6.setAgnoPublicacion(2003);
		nueva6.setPromedioRatings(4.5);
		peliculasPrueba.agregarElementoFinal(nueva6);
		nueva7.setTitulo("Life After Tomorrow ");
		nueva7.setAgnoPublicacion(2003);
		nueva7.setPromedioRatings(3.1);
		peliculasPrueba.agregarElementoFinal(nueva7);
		nueva8.setTitulo("My Favorite Martian ");
		nueva8.setAgnoPublicacion(2003);
		nueva8.setPromedioRatings(2.9);
		peliculasPrueba.agregarElementoFinal(nueva8);
		nueva9.setTitulo("River Wild, The ");
		nueva9.setAgnoPublicacion(1992);
		nueva9.setPromedioRatings(4.3);
		peliculasPrueba.agregarElementoFinal(nueva9);
		nueva10.setTitulo("Remains of the Day, The ");
		nueva10.setAgnoPublicacion(1985);
		nueva10.setPromedioRatings(1.6);
		peliculasPrueba.agregarElementoFinal(nueva10);	
	}
	
	/**
	 * Peliculas con a�o de publicaci�n y promedio de ratings repetido
	 */
	public void setupEscenario3(){
		peliculasPrueba = new ListaEncadenada<VOPelicula>();
		VOPelicula nueva1 = new VOPelicula();
		VOPelicula nueva2 = new VOPelicula();
		VOPelicula nueva3 = new VOPelicula();
		VOPelicula nueva4 = new VOPelicula();
		VOPelicula nueva5 = new VOPelicula();
		VOPelicula nueva6 = new VOPelicula();
		VOPelicula nueva7 = new VOPelicula();
		VOPelicula nueva8 = new VOPelicula();
		VOPelicula nueva9 = new VOPelicula();
		VOPelicula nueva10 = new VOPelicula();
		nueva1.setTitulo("Bill & Ted's Bogus Journey ");
		nueva1.setAgnoPublicacion(1985);
		nueva1.setPromedioRatings(3.8);
		peliculasPrueba.agregarElementoFinal(nueva1);
		nueva2.setTitulo("Boy Who Could Fly, The ");
		nueva2.setAgnoPublicacion(1976);
		nueva2.setPromedioRatings(3.2);
		peliculasPrueba.agregarElementoFinal(nueva2);
		nueva3.setTitulo("Following Sean ");
		nueva3.setAgnoPublicacion(1990);
		nueva3.setPromedioRatings(4.6);
		peliculasPrueba.agregarElementoFinal(nueva3);
		nueva4.setTitulo("How to Stuff a Wild Bikini ");
		nueva4.setAgnoPublicacion(1982);
		nueva4.setPromedioRatings(2.8);
		peliculasPrueba.agregarElementoFinal(nueva4);
		nueva5.setTitulo("Pelle the Conqueror (Pelle erobreren) ");
		nueva5.setAgnoPublicacion(1985);
		nueva5.setPromedioRatings(3.8);
		peliculasPrueba.agregarElementoFinal(nueva5);
		nueva6.setTitulo("Wanted ");
		nueva6.setAgnoPublicacion(2003);
		nueva6.setPromedioRatings(4.5);
		peliculasPrueba.agregarElementoFinal(nueva6);
		nueva7.setTitulo("Life After Tomorrow ");
		nueva7.setAgnoPublicacion(2003);
		nueva7.setPromedioRatings(4.5);
		peliculasPrueba.agregarElementoFinal(nueva7);
		nueva8.setTitulo("My Favorite Martian ");
		nueva8.setAgnoPublicacion(2003);
		nueva8.setPromedioRatings(4.5);
		peliculasPrueba.agregarElementoFinal(nueva8);
		nueva9.setTitulo("River Wild, The ");
		nueva9.setAgnoPublicacion(1992);
		nueva9.setPromedioRatings(2.6);
		peliculasPrueba.agregarElementoFinal(nueva9);
		nueva10.setTitulo("Remains of the Day, The ");
		nueva10.setAgnoPublicacion(1985);
		nueva10.setPromedioRatings(3.8);
		peliculasPrueba.agregarElementoFinal(nueva10);	
	}
	
	public void testShellSort1(){
		setupEscenario1();
		CompararPeliculas com1 = new CompararPeliculas();
		com1.determinarCriterio(CompararPeliculas.AGNO_PUBLICACION);
		ShellSort.sort(com1, null, null, peliculasPrueba);
		assertEquals("No ordeno correctamente","Remains of the Day, The ", peliculasPrueba.darElemento(9).getTitulo());
		assertEquals("No ordeno correctamente","Pelle the Conqueror (Pelle erobreren) ", peliculasPrueba.darElemento(8).getTitulo());
		assertEquals("No ordeno correctamente","Boy Who Could Fly, The ", peliculasPrueba.darElemento(7).getTitulo());
		assertEquals("No ordeno correctamente","How to Stuff a Wild Bikini ", peliculasPrueba.darElemento(6).getTitulo());
		assertEquals("No ordeno correctamente","Bill & Ted's Bogus Journey ", peliculasPrueba.darElemento(5).getTitulo());
		assertEquals("No ordeno correctamente","Following Sean ", peliculasPrueba.darElemento(4).getTitulo());
		assertEquals("No ordeno correctamente","River Wild, The ", peliculasPrueba.darElemento(3).getTitulo());
		assertEquals("No ordeno correctamente","My Favorite Martian ", peliculasPrueba.darElemento(2).getTitulo());
		assertEquals("No ordeno correctamente","Wanted ", peliculasPrueba.darElemento(1).getTitulo());
		assertEquals("No ordeno correctamente","Life After Tomorrow ", peliculasPrueba.darElemento(0).getTitulo());
	}
	
	public void testShellSort2(){
		setupEscenario2();
		CompararPeliculas com1 = new CompararPeliculas();
		CompararPeliculas com2 = new CompararPeliculas();
		com1.determinarCriterio(CompararPeliculas.AGNO_PUBLICACION);
		com2.determinarCriterio(CompararPeliculas.MEAN_RATINGS);
		ShellSort.sort(com1, com2, null, peliculasPrueba);
		assertEquals("No ordeno correctamente","Boy Who Could Fly, The ", peliculasPrueba.darElemento(9).getTitulo());
		assertEquals("No ordeno correctamente","How to Stuff a Wild Bikini ", peliculasPrueba.darElemento(8).getTitulo());
		assertEquals("No ordeno correctamente","Remains of the Day, The ", peliculasPrueba.darElemento(7).getTitulo());
		assertEquals("No ordeno correctamente","Bill & Ted's Bogus Journey ", peliculasPrueba.darElemento(6).getTitulo());
		assertEquals("No ordeno correctamente","Pelle the Conqueror (Pelle erobreren) ", peliculasPrueba.darElemento(5).getTitulo());
		assertEquals("No ordeno correctamente","Following Sean ", peliculasPrueba.darElemento(4).getTitulo());
		assertEquals("No ordeno correctamente","River Wild, The ", peliculasPrueba.darElemento(3).getTitulo());
		assertEquals("No ordeno correctamente","My Favorite Martian ", peliculasPrueba.darElemento(2).getTitulo());
		assertEquals("No ordeno correctamente","Life After Tomorrow ", peliculasPrueba.darElemento(1).getTitulo());
		assertEquals("No ordeno correctamente","Wanted ", peliculasPrueba.darElemento(0).getTitulo());
	}
	
	public void testShellSort3(){
		setupEscenario3();
		CompararPeliculas com1 = new CompararPeliculas();
		CompararPeliculas com2 = new CompararPeliculas();
		CompararPeliculas com3 = new CompararPeliculas();
		com1.determinarCriterio(CompararPeliculas.AGNO_PUBLICACION);
		com2.determinarCriterio(CompararPeliculas.MEAN_RATINGS);
		com3.determinarCriterio(CompararPeliculas.TITULO);
		ShellSort.sort(com1, com2, com3, peliculasPrueba);
		assertEquals("No ordeno correctamente","Life After Tomorrow ", peliculasPrueba.darElemento(0).getTitulo());
		assertEquals("No ordeno correctamente","My Favorite Martian ", peliculasPrueba.darElemento(1).getTitulo());
		assertEquals("No ordeno correctamente","Wanted ", peliculasPrueba.darElemento(2).getTitulo());
		assertEquals("No ordeno correctamente","River Wild, The ", peliculasPrueba.darElemento(3).getTitulo());
		assertEquals("No ordeno correctamente","Following Sean ", peliculasPrueba.darElemento(4).getTitulo());
		assertEquals("No ordeno correctamente","Bill & Ted's Bogus Journey ", peliculasPrueba.darElemento(5).getTitulo());
		assertEquals("No ordeno correctamente","Pelle the Conqueror (Pelle erobreren) ", peliculasPrueba.darElemento(6).getTitulo());
		assertEquals("No ordeno correctamente","Remains of the Day, The ", peliculasPrueba.darElemento(7).getTitulo());
		assertEquals("No ordeno correctamente","How to Stuff a Wild Bikini ", peliculasPrueba.darElemento(8).getTitulo());
		assertEquals("No ordeno correctamente","Boy Who Could Fly, The ", peliculasPrueba.darElemento(9).getTitulo());
	}
}

