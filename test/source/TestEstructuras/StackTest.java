package TestEstructuras;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase {
	
	private Stack pila;
		
	/**
	 * Crea una pila vac�a
	 */
	private void setupEscenario1(){
		pila = new Stack<String>();
	}
	
	/**
	 * Crea una pila con Strings
	 */
	private void setupEscenario2(){
		pila = new Stack<String>();
		
		pila.push("Primer intento");
		pila.push("Segundo intento");
		pila.push("Tercer intento");
	}
	
	/**
	 * Verifica el m�todo push
	 */
	public void testPush(){
		setupEscenario1();
		
		pila.push("Primer elemento");
		assertEquals("No se agrego el elemento",1,pila.size());
		pila.push("Segundo elemento");
		assertEquals("No se agrego el elemento",2,pila.size());
		assertEquals("No est� el elemento indicado", "Segundo elemento", pila.pop());
		assertEquals("No est� el elemento indicado", "Primer elemento", pila.pop());
	}
	
	/** 
	 * Verifica el m�todo Pop sobre una pila vac�a
	 */
	public void testPop1(){
		setupEscenario1();
		assertNull(pila.pop());
	}
	
	/**
	 * Verifica el m�todo Pop sobre una pila de elementos
	 */
	public void testPop2(){
		setupEscenario2();
		assertEquals("No est� el elemento indicado", "Tercer intento", pila.pop());
		assertEquals("No se elimin� el elemento",2,pila.size());
		assertEquals("No est� el elemento indicado", "Segundo intento", pila.pop());
		assertEquals("No se elimin� el elemento",1,pila.size());
		assertEquals("No est� el elemento indicado", "Primer intento", pila.pop());
		assertEquals("No se elimin� el elemento",0,pila.size());
	}

	/**
	 * Verifica el m�todo isEmpty sobre una pila vac�a
	 */
	public void testIsEmpty1(){
		setupEscenario1();
		assertTrue("La pila no est� vac�a", pila.isEmpty());
	}
	
	/**
	 * Verifica el m�todo isEmpty sobre una pila llena
	 */
	public void testIsEmpty2(){
		setupEscenario2();
		assertFalse("La pila est� vac�a", pila.isEmpty());
	}
	
	/**
	 * Verifica el m�todo isEmpty sobre una pila despu�s de vaciarse
	 */
	public void testIsEmpty3(){
		setupEscenario2();
		pila.pop();
		pila.pop();
		pila.pop();
		assertTrue("La pila no est� vac�a", pila.isEmpty());
	}
	
	/**
	 * Verifica el m�todo size sobre una pila vac�a
	 */
	public void testSize1(){
		setupEscenario1();
		assertEquals("Cantidad errada de elementos", 0, pila.size());
	}
	
	/**
	 * Verifica el m�todo size sobre una pila de tres elementos
	 */
	public void testSize2(){
		setupEscenario2();
		assertEquals("Cantidad errada de elementos", 3, pila.size());
	}

}
