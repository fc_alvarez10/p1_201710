package TestEstructuras;

import junit.framework.TestCase;

import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase{

private ListaDobleEncadenada<String> lista;
	
	/**
	 * Crea una lista vac�a
	 */
	private void setupEscenario1(){
		lista = new ListaDobleEncadenada<String>();
	}
	
	/**
	 * Crea una lista con 3 Strings
	 */
	private void setupEscenario2(){
		lista = new ListaDobleEncadenada<String>();
		
		lista.agregarElementoFinal("Primer intento");
		lista.agregarElementoFinal("Segundo intento");
		lista.agregarElementoFinal("Tercer intento");
	}
	
	/**
	 * Verifica el m�todo AgregarElementoFinal
	 */
	public void testAgregarElementoFinal(){
		setupEscenario1();
		
		lista.agregarElementoFinal("Primer intento");
		assertEquals("No se agrego el elemento",1,lista.darNumeroElementos());
		lista.agregarElementoFinal("Segundo intento");
		assertEquals("No se agrego el elemento",2,lista.darNumeroElementos());
		assertEquals("No est� el elemento indicado", "Primer intento", lista.darElemento(0));
		assertEquals("No est� el elemento indicado", "Segundo intento", lista.darElemento(1));
	}
	
	/**
	 * Verifica el m�todo darNumeroElementos sobre una lista vac�a
	 */
	public void testDarNumeroElementos1(){
		setupEscenario1();
		assertEquals("Cantidad errada de elementos", 0, lista.darNumeroElementos());
	}
	
	/**
	 * Verifica el m�todo darNumeroElementos sobre una lista de tres elementos
	 */
	public void testDarNumeroElementos2(){
		setupEscenario2();
		assertEquals("Cantidad errada de elementos", 3, lista.darNumeroElementos());
	}
	
	/**
	 * Verifica el m�todo darNumeroElementos sobre una lista vac�a
	 */
	public void testDarElemento1(){
		setupEscenario1();
		assertNull("Debe ser null", lista.darElemento(0));
		assertNull("Debe ser null", lista.darElemento(-1));
	}
	
	/**
	 * Verifica el m�todo darNumeroElementos sobre una lista de tres elementos
	 */
	public void testDarElemento2(){
		setupEscenario2();
		assertNull("Debe ser null", lista.darElemento(-1));
		assertNull("Debe ser null", lista.darElemento(3));
		assertEquals("No est� el elemento indicado", "Primer intento", lista.darElemento(0));
		assertEquals("No est� el elemento indicado", "Segundo intento", lista.darElemento(1));
		assertEquals("No est� el elemento indicado", "Tercer intento", lista.darElemento(2));
	}
	
	/**
	 * Verifica el m�todo darPosicionActual
	 */
	public void testDarElementoPosicionActual1(){
		setupEscenario1();
		assertNull("Debe ser null", lista.darElementoPosicionActual());
		lista.agregarElementoFinal("Primer elemento");
		assertNotNull("No debe ser null", lista.darElementoPosicionActual());
		assertEquals("No es el elemento correcto", "Primer elemento", lista.darElementoPosicionActual());
	}
	
	/**
	 * Verifica el m�todo darPosicionActual
	 */
	public void testDarElementoPosicionActual2(){
		setupEscenario2();
		assertNotNull("Debe ser null", lista.darElementoPosicionActual());
		assertEquals("No es el elemento correcto", "Primer intento", lista.darElementoPosicionActual());
		lista.avanzarSiguientePosicion();
		assertEquals("No es el elemento correcto", "Segundo intento", lista.darElementoPosicionActual());
		lista.avanzarSiguientePosicion();
		assertEquals("No es el elemento correcto", "Tercer intento", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
		assertEquals("No es el elemento correcto", "Segundo intento", lista.darElementoPosicionActual());
		lista.agregarElementoFinal("Cuarto intento");
		lista.darElemento(3);
		assertEquals("No es el elemento correcto", "Cuarto intento", lista.darElementoPosicionActual());
	}
	
	/**
	 * Verifica el m�todo retrocederPosicionAnterior
	 */
	public void testRetrocederPosicionAnterior1(){
		setupEscenario1();
		assertFalse("Debe ser false", lista.retrocederPosicionAnterior());
		lista.agregarElementoFinal("Primer elemento");
		assertFalse("Debe ser False", lista.retrocederPosicionAnterior()); 
	}
	
	/**
	 * Verifica el m�todo retrocederPosicionAnterior
	 */
	public void testRetrocederPosicionAnterior2(){
		setupEscenario2();
		lista.darElemento(1);
		assertTrue("Debe ser true", lista.retrocederPosicionAnterior());
		assertEquals("No es el elemento correcto", "Primer intento", lista.darElementoPosicionActual());
		assertFalse("Debe ser False", lista.retrocederPosicionAnterior()); 
		assertEquals("No es el elemento correcto", "Primer intento", lista.darElementoPosicionActual());
	}
	
	/**
	 * Verifica el m�todo avanzarSiguientePosicion
	 */
	public void testavanzarSiguientePosicion1(){
		setupEscenario1();
		assertFalse("Debe ser false", lista.avanzarSiguientePosicion());
		lista.agregarElementoFinal("Primer elemento");
		assertFalse("Debe ser False", lista.avanzarSiguientePosicion()); 
	}
	
	/**
	 * Verifica el m�todo avanzarSiguientePosicion
	 */
	public void testavanzarSiguientePosicion2(){
		setupEscenario2();
		assertTrue("Debe ser true", lista.avanzarSiguientePosicion());
		assertEquals("No es el elemento correcto", "Segundo intento", lista.darElementoPosicionActual());
		assertTrue("Debe ser true", lista.avanzarSiguientePosicion());
		assertEquals("No es el elemento correcto", "Tercer intento", lista.darElementoPosicionActual());
		assertFalse("Debe ser False", lista.avanzarSiguientePosicion()); 
		assertEquals("No es el elemento correcto", "Tercer intento", lista.darElementoPosicionActual());
	}

}
