package TestEstructuras;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase{
	
	private Queue cola;
	
	/**
	 * Crea una cola vac�a
	 */
	private void setupEscenario1(){
		cola = new Queue<String>();
	}
	
	/**
	 * Crea una cola con Strings
	 */
	private void setupEscenario2(){
		cola = new Queue<String>();
		
		cola.enqueue("Primer intento");
		cola.enqueue("Segundo intento");
		cola.enqueue("Tercer intento");
	}
	
	/**
	 * Verifica el m�todo enqueue
	 */
	public void testEnqueue(){
		setupEscenario1();
		
		cola.enqueue("Primer elemento");
		assertEquals("No se agrego el elemento",1,cola.size());
		cola.enqueue("Segundo elemento");
		assertEquals("No se agrego el elemento",2,cola.size());
		assertEquals("No est� el elemento indicado", "Primer elemento", cola.dequeue());
		assertEquals("No est� el elemento indicado", "Segundo elemento", cola.dequeue());
	}
	
	/** 
	 * Verifica el m�todo dequeue sobre una lista vac�a
	 */
	public void testDequeue1(){
		setupEscenario1();
		assertNull(cola.dequeue());
	}
	
	/**
	 * Verifica el m�todo dequeue sobre una cola de elementos
	 */
	public void testDequeue2(){
		setupEscenario2();
		assertEquals("No est� el elemento indicado", "Primer intento", cola.dequeue());
		assertEquals("No se elimin� el elemento",2,cola.size());
		assertEquals("No est� el elemento indicado", "Segundo intento", cola.dequeue());
		assertEquals("No se elimin� el elemento",1,cola.size());
		assertEquals("No est� el elemento indicado", "Tercer intento", cola.dequeue());
		assertEquals("No se elimin� el elemento",0,cola.size());
	}

	/**
	 * Verifica el m�todo isEmpty sobre una cola vac�a
	 */
	public void testIsEmpty1(){
		setupEscenario1();
		assertTrue("La cola no est� vac�a", cola.isEmpty());
	}
	
	/**
	 * Verifica el m�todo isEmpty sobre una cola llena
	 */
	public void testIsEmpty2(){
		setupEscenario2();
		assertFalse("La cola est� vac�a", cola.isEmpty());
	}
	
	/**
	 * Verifica el m�todo isEmpty sobre una cola despu�s de vaciarse
	 */
	public void testIsEmpty3(){
		setupEscenario2();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		assertTrue("La cola no est� vac�a", cola.isEmpty());
	}
	
	/**
	 * Verifica el m�todo size sobre una cola vac�a
	 */
	public void testSize1(){
		setupEscenario1();
		assertEquals("Cantidad errada de elementos", 0, cola.size());
	}
	
	/**
	 * Verifica el m�todo size sobre una cola de tres elementos
	 */
	public void testSize2(){
		setupEscenario2();
		assertEquals("Cantidad errada de elementos", 3, cola.size());
	}

}
