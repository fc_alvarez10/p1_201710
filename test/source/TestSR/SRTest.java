package TestSR;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;
import model.logic.SistemaRecomendacionPeliculas;

public class SRTest extends TestCase {

	private SistemaRecomendacionPeliculas SR;

	public void setupEscenario(){
		SR = new SistemaRecomendacionPeliculas("docs/moviesPrueba.csv","docs/ratingsPrueba.csv","docs/tagsPrueba.csv");
	}

	public void testSizeMovies(){
		setupEscenario();
		assertEquals("No estan completas", 8, SR.sizeMoviesSR());
	}

	public void testSizeUsers(){
		setupEscenario();
		assertEquals("No estan completas", 6, SR.sizeUsersSR());
	}

	public void testSizeTags(){
		setupEscenario();
		assertEquals("No estan completas", 12, SR.sizeTagsSR());
	}


}
