package model.sort;

import java.util.Comparator;

import model.data_structures.ILista;

public class ShellSort {

	public static <T> void sort(Comparator<T> comp, Comparator<T> comp2, Comparator<T> comp3, ILista<T> lista){
		int N = lista.darNumeroElementos();
		int h = 1;
		while (h < N/7) h = 7*h + 1;
		
		while (h >= 1)
		{
			for(int i = h; i < N; i++)
			{
				for (int j = i; j >= h && less(comp, comp2, comp3, lista.darElemento(j), lista.darElemento(j-h)); j -= h)
				exch(lista,j,j-h);
			}
			h = h /7;
		}
	}

	private static <T> void exch(ILista<T> lista, int i, int j) {
		// TODO Auto-generated method stub
		T swap = lista.darElemento(i);
		lista.cambiarElemento(i, lista.darElemento(j));
		lista.cambiarElemento(j, swap);
	}

	private static <T> boolean less(Comparator<T> orden, Comparator<T> orden2, Comparator<T> orden3, T elem1, T elem2) {
		// TODO Auto-generated method stub
		if (orden.compare(elem1, elem2) == 0 && orden2 != null){
			if (orden2.compare(elem1, elem2) == 0 && orden3 != null )
				return orden3.compare(elem1, elem2) > 0 ;
			else
				return orden2.compare(elem1, elem2) > 0;
		}return orden.compare(elem1, elem2) > 0;
			
	}
}
