package model.sort;

import java.util.Comparator;

import model.vo.VOUsuarioConteo;

public class CompararUsuarioConteo implements Comparator<VOUsuarioConteo>{

	@Override
	public int compare(VOUsuarioConteo o1, VOUsuarioConteo o2) {
		// TODO Auto-generated method stub
		return o1.getConteo() - o2.getConteo();
	}

}
