package model.sort;

import java.util.Comparator;

import model.vo.VOUsuario;

public class CompararUsuarios implements Comparator<VOUsuario> {

	public final static int NUM_RATINGS = 1;

	public final static int TIME_FIRST_RAT = 2;

	public final static int ID = 3;

	public final static int DIFF_RATINGS = 4;

	private int criterio;

	public void determinarCriterio(int crit){
		criterio = crit;
	}

	@Override
	public int compare(VOUsuario o1, VOUsuario o2) {
		// TODO Auto-generated method stub
		switch (criterio){
		case 1:
			return o1.getNumRatings() - o2.getNumRatings();
		case 2:
			return (int)(o1.getPrimerTimestamp() - o2.getPrimerTimestamp()) ;
		case 3:
			return (int)(o1.getIdUsuario() - o2.getIdUsuario());
		case 4:
			return (int)(10*o2.getDiferenciaOpinion() - 10*o1.getDiferenciaOpinion());
		default:
			return 0;
		}
	}

}
