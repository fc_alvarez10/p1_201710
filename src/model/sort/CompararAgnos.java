package model.sort;

import java.util.Comparator;

import model.vo.VOAgnoPelicula;

public class CompararAgnos implements Comparator<VOAgnoPelicula>{

	@Override
	public int compare(VOAgnoPelicula o1, VOAgnoPelicula o2) {
		// TODO Auto-generated method stub
		return o2.getAgno() - o1.getAgno();
	}
	

}
