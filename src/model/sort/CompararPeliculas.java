package model.sort;

import java.util.Comparator;

import model.vo.VOPelicula;

public class CompararPeliculas implements Comparator<VOPelicula> {

	public final static int NUM_RATINGS = 1;
	
	public final static int AGNO_PUBLICACION = 2;
	
	public final static int MEAN_RATINGS = 3;
	
	public final static int TITULO = 4;
	
	public final static int NUM_TAGS = 5;
	
	public final static int DIFF_RATINGS = 6;
	
	private int criterio;
	
	public void determinarCriterio(int crit){
		 criterio = crit;
	}
	
	@Override
	public int compare(VOPelicula arg0, VOPelicula arg1) {
		// TODO Auto-generated method stub
		switch (criterio){
		case 1:
			return arg0.getNumeroRatings() - arg1.getNumeroRatings();
		case 2:
			return arg0.getAgnoPublicacion() - arg1.getAgnoPublicacion();
		case 3:
			return (int)(10*arg0.getPromedioRatings() - 10*arg1.getPromedioRatings());
		case 4:
			return arg1.getTitulo().compareTo(arg0.getTitulo());
		case 5:
			return arg0.getNumeroTags() - arg1.getNumeroTags();
		case 6:
			return (int)(10*arg1.getDiferenciaRats() - 10*arg0.getDiferenciaRats());
		default:
			return 0;
		//case 6:
		}	
	}

}
