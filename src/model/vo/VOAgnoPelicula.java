package model.vo;

import model.data_structures.ILista;

public class VOAgnoPelicula {

	private int agno;
	private ILista<VOPelicula> peliculasPorPromedioRatings;
	
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculasPorPromedioRatings;
	}
	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculasPorPromedioRatings = peliculas;
	}
	
	
}
