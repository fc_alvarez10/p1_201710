package model.vo;

import model.data_structures.ILista;

public class VOGeneroPelicula {
	
	private String genero;
	
	private ILista<VOPelicula> peliculas;
	
	/**
	 * lista de peliculas ordenadas por mayor numero de ratings
	 */
	private ILista<VOPelicula> peliculasPorPopularidad;

	/**
	 * lista de peliculas ordenadas por calificación promedio
	 */
	private ILista<VOPelicula> peliculasPorPromedioRatings;
	
	/**
	 * lista de peliculas ordenadas por fecha
	 */
	private ILista<VOPelicula> peliculasPorFecha;
	
	/**
	 * lista de peliculas ordenadas por mayor numero de tags
	 */
	private ILista<VOPelicula> peliculasPorNumeroTags;
	
	
	
	public ILista<VOPelicula> getPeliculasPorNumeroTags() {
		return peliculasPorNumeroTags;
	}

	public void setPeliculasPorNumeroTags(ILista<VOPelicula> peliculasPorNumeroTags) {
		this.peliculasPorNumeroTags = peliculasPorNumeroTags;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 
	
	public ILista<VOPelicula> getPeliculasPorPopularidad() {
		return peliculasPorPopularidad;
	}

	public void setPeliculasPorPopularidad(ILista<VOPelicula> peliculasPorPopularidad) {
		this.peliculasPorPopularidad = peliculasPorPopularidad;
	}

	public ILista<VOPelicula> getPeliculasPorFecha() {
		return peliculasPorFecha;
	}

	public void setPeliculasPorFecha(ILista<VOPelicula> peliculasPorFecha) {
		this.peliculasPorFecha = peliculasPorFecha;
	}

	public ILista<VOPelicula> getPeliculasPorPromedioRatings() {
		return peliculasPorPromedioRatings;
	}

	public void setPeliculasPorPromedioRatings(ILista<VOPelicula> peliculasPorPromedioRatings) {
		this.peliculasPorPromedioRatings = peliculasPorPromedioRatings;
	}


}
