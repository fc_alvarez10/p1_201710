package model.vo;

import model.data_structures.ILista;

public class VOPelicula {

	private int id;
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;
	private double sumatoriaRatings;
	private ILista<String> tagsAsociados;
	private ILista<VOTag> tagsAsociadosVO;
	private ILista<String> generosAsociados;
	private ILista<VORating> ratingsAsociados;
	private double diferenciaRats;

	public ILista<VOTag> getTagsAsociadosVO() {
		return tagsAsociadosVO;
	}

	public void setTagsAsociadosVO(ILista<VOTag> tagsAsociadosVO) {
		this.tagsAsociadosVO = tagsAsociadosVO;
	}
	
	public ILista<VORating> getRatingsAsociados() {
		return ratingsAsociados;
	}

	public void setRatingsAsociados(ILista<VORating> ratingsAsociados) {
		this.ratingsAsociados = ratingsAsociados;
	}

	public double getSumatoriaRatings() {
		return sumatoriaRatings;
	}

	public void setSumatoriaRatings(double sumatoriaRatings) {
		this.sumatoriaRatings = sumatoriaRatings;
	}

	public int getId(){
		return id;
	}

	public void setId(int pId){
		id=pId;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public double getDiferenciaRats() {
		return diferenciaRats;
	}

	public void setDiferenciaRats(double diferenciaRats) {
		this.diferenciaRats = diferenciaRats;
	}


}
