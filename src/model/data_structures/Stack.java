package model.data_structures;

public class Stack<T> implements IStack<T> {
	
	private ListaEncadenada<T> lista;
	
	public Stack(){
		lista = new ListaEncadenada<T>();
	}

	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		lista.agregarAlPrincipio(item);
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return lista.eliminarPrimero();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}
	

}
