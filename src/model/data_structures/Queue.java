package model.data_structures;

public class Queue<T> implements IQueue<T>{

	private ListaEncadenada<T> lista;
	
	public Queue(){
		lista = new ListaEncadenada<T>();
	}
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		lista.agregarElementoFinal(item);
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return lista.eliminarPrimero();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}

}
