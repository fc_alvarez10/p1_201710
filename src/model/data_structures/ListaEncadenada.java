package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;
	private NodoSencillo<T> actual;
	private NodoSencillo<T> ultimo;
	private int size=0;


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		actual = primero;
		Iterator iter = new Iterator<T>() {

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return (actual.darSiguiente() != null);
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				NodoSencillo<T> temp = actual;
				actual = actual.darSiguiente();
				return temp.darNodo();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
			}
		};
		return iter;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		if (size ==0 ){	primero = nuevo;
		actual = primero;}
		else 
			ultimo.cambiarSiguiente(nuevo);
		ultimo = nuevo;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int cont = 0;
		actual = primero;
		while (cont < pos){
			actual = actual.darSiguiente();	
			cont++;
		}
		return pos < 0 ? null : (actual == null ? null : actual.darNodo());
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual == null ? null : actual.darNodo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if (actual == null || actual.darSiguiente() == null) return false;
		actual = actual.darSiguiente();
		return true;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if (actual == primero) return false;
		NodoSencillo<T> act = actual;
		actual = primero;
		while (actual.darSiguiente() != act){
			actual = actual.darSiguiente();
		}
		return true;
	}

	public T eliminarPrimero(){
		if (primero != null){
			NodoSencillo<T> retorno = primero;
			//NodoSencillo<T> act = actual;
			actual = primero;
			avanzarSiguientePosicion();
			primero = actual;
			//actual = act;
			size--;
			return retorno.darNodo();
		}
		return null;
	}

	public void agregarAlPrincipio(T elem){
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		nuevo.cambiarSiguiente(primero);
		primero = nuevo;
		actual = primero;
		if (size == 0) ultimo = primero;
		size++;
	}
	
	public void cambiarElemento(int pos, T nuevo){
			// TODO Auto-generated method stub
			int cont = 0;
			actual = primero;
			while (cont < pos){
				actual = actual.darSiguiente();	
				cont++;
			}
			if (actual != null && pos >= 0) actual.cambiarElemento(nuevo);
	}

}
