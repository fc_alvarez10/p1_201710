package model.logic;

import model.data_structures.*;
import model.sort.*;
import model.vo.*;

import java.io.*;
import java.util.Iterator;

import api.ISistemaRecomendacionPeliculas;


public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private CatalogoGeneros catalogoDeGeneros;

	private CatalogoUsuarios catalogoDeUsuarios;

	private CatalogoAgnos catalogoDeAgnos;

	private int numeroTags;
	
	private IStack<VOOperacion> operaciones;

	public SistemaRecomendacionPeliculas(String rutaMovies, String rutaRatings, String rutaTags){
		operaciones = new Stack<VOOperacion>();
		misPeliculas=new ListaEncadenada<>();
		catalogoDeGeneros=new CatalogoGeneros();
		catalogoDeUsuarios=new CatalogoUsuarios();
		catalogoDeAgnos=new CatalogoAgnos();
		numeroTags=0;
		cargarPeliculasSR(rutaMovies);
		cargarRatingsSR(rutaRatings);
		cargarTagsSR(rutaTags);
		catalogoDeGeneros.ordenarListas();
		ordenarRatingsXMovie();
		ordenarTagsXMovie();
		catalogoDeUsuarios.ordenarUsuarios();
	}

	public SistemaRecomendacionPeliculas(){
		operaciones = new Stack<VOOperacion>();
		misPeliculas=new ListaEncadenada<>();
		catalogoDeGeneros=new CatalogoGeneros();
		catalogoDeUsuarios=new CatalogoUsuarios();
		catalogoDeAgnos=new CatalogoAgnos();
		numeroTags=0;
		cargarPeliculasSR("docs/movies.csv");
		cargarRatingsSR("docs/ratings.csv");
		cargarTagsSR("docs/tags.csv");
		catalogoDeGeneros.ordenarListas();
		ordenarRatingsXMovie();
		ordenarTagsXMovie();
		catalogoDeUsuarios.ordenarUsuarios();
	}
	
	public void ordenarTagsXMovie(){
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		CompararTags comp = new CompararTags();
		while(iter.hasNext()){
			VOPelicula act = iter.next();
			ShellSort.sort(comp, null, null, act.getTagsAsociadosVO());
		}
	}
	
	public void ordenarRatingsXMovie(){
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		CompararRatings comp = new CompararRatings();
		while(iter.hasNext()){
			VOPelicula act = iter.next();
			ShellSort.sort(comp, null, null, act.getRatingsAsociados());
		}
	}

	public ISistemaRecomendacionPeliculas crearSR(){
		long tinicio = System.currentTimeMillis();
		SistemaRecomendacionPeliculas nueva = new SistemaRecomendacionPeliculas();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return nueva;
	}


	public boolean cargarPeliculasSR(String rutaPeliculas) {
		long tinicio = System.currentTimeMillis();
		BufferedReader br = null;
		boolean carga=false;
		try {
			br =new BufferedReader(new FileReader(rutaPeliculas));
			String line = br.readLine();
			line = br.readLine();
			while (line != null) {
				String[] separacion = line.split(",");
				if (separacion.length != 3) {
					separacion = line.split("\"\"");
					if (separacion[0].startsWith("\"") && separacion[0].endsWith(","))
					separacion[0] = separacion[0].substring(1, separacion[0].indexOf(","));
				}
				VOPelicula nueva = new VOPelicula();
				int agno = 0;
				//System.out.println(separacion[0]);
				int id=Integer.parseInt(separacion[0]);
				if(id>0){
					nueva.setId(id);
				}
				if (separacion[1].lastIndexOf("(") < 0)
					nueva.setTitulo(separacion[1]);
				else {
					nueva.setTitulo(separacion[1].substring(0,separacion[1].lastIndexOf("(")));
					agno = Integer.parseInt(separacion[1].substring(separacion[1].lastIndexOf("(")+1,separacion[1].lastIndexOf("(")+5));
					nueva.setAgnoPublicacion(agno); }
				if (separacion[2].startsWith("(") == false) {
					String[] gen = separacion[2].split("|");
					ListaEncadenada<String> generos = new ListaEncadenada<String>();
					for (int i = 0; i < gen.length; i++){
						generos.agregarElementoFinal(gen[i]);

						//-------- Guarda las peliculas por genero
						if(!catalogoDeGeneros.generoExiste(gen[i])){
							//agrega un nuevo genero y a este le agrega la pelicula
							VOGeneroPelicula nuevoGenero=new VOGeneroPelicula();
							nuevoGenero.setGenero(gen[i]);
							ILista<VOPelicula> nuevalistaPeliculasGenero=new ListaEncadenada<>();
							nuevalistaPeliculasGenero.agregarElementoFinal(nueva);
							nuevoGenero.setPeliculas(nuevalistaPeliculasGenero);
							catalogoDeGeneros.agregarNuevoGenero(nuevoGenero);

						}else{
							//agrega la nueva pelicula al genero existente
							VOGeneroPelicula x=catalogoDeGeneros.buscarGenero(gen[i]);
							ILista<VOPelicula> lista=x.getPeliculas();
							lista.agregarElementoFinal(nueva);
							x.setPeliculas(lista);

						}
						//----------------------------------------------
					}
					nueva.setGenerosAsociados(generos);
					carga=true;
				}
				misPeliculas.agregarElementoFinal(nueva);

				//-----------Guarda las peliculas por a�o
				if(!catalogoDeAgnos.agnoExiste(agno)){
					//crea el agno y agrega la pelicula
					VOAgnoPelicula nuevoAgno=new VOAgnoPelicula();
					nuevoAgno.setAgno(agno);
					ILista<VOPelicula> listaPeliculas=new ListaEncadenada<>();
					listaPeliculas.agregarElementoFinal(nueva);
					nuevoAgno.setPeliculas(listaPeliculas);
					catalogoDeAgnos.agregarNuevoAgno(nuevoAgno);
				}else{
					//agrega la pelicula al agno existente
					VOAgnoPelicula act=catalogoDeAgnos.buscarAgno(agno);
					ILista<VOPelicula> listaPeliculas = act.getPeliculas();
					listaPeliculas.agregarElementoFinal(nueva);
					act.setPeliculas(listaPeliculas);
				}
				line = br.readLine();
			}
			long tfin = System.currentTimeMillis();
			agregarOperacionSR(methodName(), tinicio, tfin);
			return carga;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public boolean cargarRatingsSR(String rutaRatings){
		long tinicio = System.currentTimeMillis();
		BufferedReader br=null;
		try{

			br =new BufferedReader(new FileReader(rutaRatings));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion = line.split(",");
				int userId=Integer.parseInt(separacion[0]);
				int movieId=Integer.parseInt(separacion[1]);
				double rating=Double.parseDouble(separacion[2]);
				long timestampRating=Long.parseLong(separacion[3]);

				VORating nuevoRating=new VORating();
				nuevoRating.setIdPelicula(movieId);
				nuevoRating.setIdUsuario(userId);
				nuevoRating.setRating(rating);
				nuevoRating.setTimeStamp(timestampRating);
				//------------------Agrega el rating a la pelicula
				VOPelicula peliculaActual=buscarPelicula(movieId);
				if(peliculaActual!=null){
					peliculaActual.setNumeroRatings(peliculaActual.getNumeroRatings()+1);
					peliculaActual.setSumatoriaRatings(peliculaActual.getSumatoriaRatings()+rating);
					peliculaActual.setPromedioRatings(peliculaActual.getSumatoriaRatings()/peliculaActual.getNumeroRatings());
					if(peliculaActual.getRatingsAsociados()!=null){
						ILista<VORating> listaRating=peliculaActual.getRatingsAsociados();
						listaRating.agregarElementoFinal(nuevoRating);
						peliculaActual.setRatingsAsociados(listaRating);
					}else{
						ILista<VORating> listaRating=new ListaEncadenada<>();
						listaRating.agregarElementoFinal(nuevoRating);
						peliculaActual.setRatingsAsociados(listaRating);
					}
				}

				//----------------Agrega el rating al usuario
				VOUsuario usuarioActual=catalogoDeUsuarios.buscarUsuario(userId);
				if(usuarioActual!=null){
					// el usuario ya existe
					usuarioActual.setNumRatings((usuarioActual.getNumRatings())+1);
					if(timestampRating<usuarioActual.getPrimerTimestamp()){
						usuarioActual.setPrimerTimestamp(timestampRating);
					}

					if(peliculaActual!=null){

						VOGeneroUsuario genUsu=catalogoDeGeneros.buscarGeneroUsuario(peliculaActual.getGenerosAsociados().darElemento(0));
						VOUsuarioConteo nuevo=new VOUsuarioConteo();
						nuevo.setConteo(usuarioActual.getNumRatings());
						nuevo.setIdUsuario(usuarioActual.getIdUsuario());
						if(genUsu!=null){
							ILista<VOUsuarioConteo> listaAct=genUsu.getUsuarios();
							listaAct.agregarElementoFinal(nuevo);
							genUsu.setUsuarios(listaAct);
						}else{
							genUsu=new VOGeneroUsuario();
							genUsu.setGenero(peliculaActual.getGenerosAsociados().darElemento(0));
							ILista<VOUsuarioConteo> nuevaLista=new ListaEncadenada<>();
							nuevaLista.agregarElementoFinal(nuevo);
							genUsu.setUsuarios(nuevaLista);
						}

					}

				}else{
					// crea el usuario y le agrega el rating
					VOUsuario nuevo=new VOUsuario();
					nuevo.setIdUsuario(userId);
					nuevo.setPrimerTimestamp(timestampRating);
					nuevo.setNumRatings(1);
					ILista<VOUsuario> listaUsuarios=catalogoDeUsuarios.getListaUsuarios();
					if(listaUsuarios!=null){
						listaUsuarios.agregarElementoFinal(nuevo);
						catalogoDeUsuarios.setListaUsuarios(listaUsuarios);
					}else{
						listaUsuarios=new ListaEncadenada<>();
						listaUsuarios.agregarElementoFinal(nuevo);
						catalogoDeUsuarios.setListaUsuarios(listaUsuarios);
					}
					VOGeneroUsuario genUsu=catalogoDeGeneros.buscarGeneroUsuario(peliculaActual.getGenerosAsociados().darElemento(0));
					VOUsuarioConteo usuarioConteo=new VOUsuarioConteo();
					usuarioConteo.setConteo(1);
					usuarioConteo.setIdUsuario(userId);
					if(genUsu!=null){
						ILista<VOUsuarioConteo> listaAct=genUsu.getUsuarios();
						listaAct.agregarElementoFinal(usuarioConteo);
						genUsu.setUsuarios(listaAct);
					}else{
						genUsu=new VOGeneroUsuario();
						genUsu.setGenero(peliculaActual.getGenerosAsociados().darElemento(0));
						ILista<VOUsuarioConteo> nuevaLista=new ListaEncadenada<>();
						nuevaLista.agregarElementoFinal(usuarioConteo);
						genUsu.setUsuarios(nuevaLista);
					}

				}
				line=br.readLine();
			}
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return true;
	}

	public boolean cargarTagsSR(String rutaTags){
		long tinicio = System.currentTimeMillis();
		BufferedReader br=null;
		boolean carga=false;
		try{

			br =new BufferedReader(new FileReader(rutaTags));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion = line.split(",");
				int userId=Integer.parseInt(separacion[0]);
				int movieId=Integer.parseInt(separacion[1]);
				String tag=separacion[2];
				Long timestampTag=Long.parseLong(separacion[3]);
				VOTag nuevoTag = new VOTag();
				nuevoTag.setTag(tag);
				nuevoTag.setTimestamp(timestampTag);
				if(!catalogoDeUsuarios.existeUsuario(userId)){
					//crea un nuevo usuario 
					VOUsuario nuevoUsuario=new VOUsuario();
					ILista<VOTag> nuevaListaTags=new ListaEncadenada<>();
					nuevaListaTags.agregarElementoFinal(nuevoTag);
					nuevoUsuario.setListaTags(nuevaListaTags);
					catalogoDeUsuarios.agregarUsuarioNuevo(nuevoUsuario);

				}else{
					//agrega el tag al usuario
					VOUsuario act=catalogoDeUsuarios.buscarUsuario(userId);
					ILista<VOTag> listaTags=act.getListaTags();
					listaTags.agregarElementoFinal(nuevoTag);
					act.setListaTags(listaTags);
				}

				VOPelicula act=buscarPelicula(movieId);
				if(act!=null){
					VOUsuarioGenero usuGen=catalogoDeUsuarios.buscarUsuarioGenero(userId);

					if(usuGen!=null){
						//existe el usuario
						String generoTag=act.getGenerosAsociados().darElemento(0);
						VOGeneroTag genTag=catalogoDeUsuarios.buscarGeneroTag(usuGen, generoTag);
						if( genTag!=null){
							//existe el genero tag y se agrega el tag
							ILista<String> listaTags=genTag.getTags();
							listaTags.agregarElementoFinal(tag);
							genTag.setTags(listaTags);
						}else{
							//se crea el gen tag y se agrega
							ILista<VOGeneroTag> listaGenTag=usuGen.getListaGeneroTags();
							VOGeneroTag nuevoGenTag= new VOGeneroTag();
							nuevoGenTag.setGenero(generoTag);
							ILista<String> listaTag=new ListaEncadenada<>();
							listaTag.agregarElementoFinal(tag);
							nuevoGenTag.setTags(listaTag);
							listaGenTag.agregarElementoFinal(nuevoGenTag);
							usuGen.setListaGeneroTags(listaGenTag);	
						}


					}else{
						//no existe el usuario
						VOUsuarioGenero nuevoUsuario=new VOUsuarioGenero();
						nuevoUsuario.setIdUsuario(userId);
						ILista<VOGeneroTag> nuevaListaGeneroTag=new ListaEncadenada<>();
						VOGeneroTag nuevoGenero=new VOGeneroTag();
						nuevoGenero.setGenero(act.getGenerosAsociados().darElemento(0));
						ILista<String> listatag=new ListaEncadenada<>();
						listatag.agregarElementoFinal(tag);
						nuevoGenero.setTags(listatag);
						nuevaListaGeneroTag.agregarElementoFinal(nuevoGenero);
						nuevoUsuario.setListaGeneroTags(nuevaListaGeneroTag);
						catalogoDeUsuarios.agregarUsuarioGeneroNuevo(nuevoUsuario);


					}

					if(act.getTagsAsociados()!=null){
						//agrega el tag a la lista existente
						ILista<String> ListaTagsPelicula=act.getTagsAsociados();
						ListaTagsPelicula.agregarElementoFinal(tag);
						act.setTagsAsociados(ListaTagsPelicula);
						act.setNumeroTags(ListaTagsPelicula.darNumeroElementos());
					}else{

						//crea la lista de tags y agrega el nuevo tag
						ILista<String> ListaTagsPelicula= new ListaEncadenada<>();
						ListaTagsPelicula.agregarElementoFinal(tag);
						act.setTagsAsociados(ListaTagsPelicula);
						act.setNumeroTags(ListaTagsPelicula.darNumeroElementos());
					}

					if(act.getTagsAsociadosVO()!=null){
						//agrega el tag a la lista vo  existente
						ILista<VOTag> ListaTagsPelicula=act.getTagsAsociadosVO();
						ListaTagsPelicula.agregarElementoFinal(nuevoTag);
						act.setTagsAsociadosVO(ListaTagsPelicula);
					}else{

						//crea la lista de tags y agrega el nuevo votag
						ILista<VOTag> ListaTagsPelicula= new ListaEncadenada<>();
						ListaTagsPelicula.agregarElementoFinal(nuevoTag);
						act.setTagsAsociadosVO(ListaTagsPelicula);
					}

				}
				numeroTags++;
				line=br.readLine();
			}
			carga=true;

		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return carga;
	}

	@Override
	public int sizeMoviesSR() {
		long tinicio = System.currentTimeMillis();
		int ret = misPeliculas.darNumeroElementos();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public int sizeUsersSR() {
		long tinicio = System.currentTimeMillis();
		int ret = catalogoDeUsuarios.getListaUsuarios().darNumeroElementos();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public int sizeTagsSR() {
		long tinicio = System.currentTimeMillis();
		int ret = numeroTags;
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		long tinicio = System.currentTimeMillis();
		ILista<VOGeneroPelicula> ret = catalogoDeGeneros.nPeliculasMasPopulares(n);
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		long tinicio = System.currentTimeMillis();
		ILista<VOPelicula> ret = catalogoDeAgnos.peliculasConcatenadas();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		long tinicio = System.currentTimeMillis();
		ILista<VOGeneroPelicula> ret = catalogoDeGeneros.peliculasMejorCalificacionPromedio();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		long tinicio = System.currentTimeMillis();
		ILista<VOGeneroPelicula> ret = catalogoDeGeneros.peliculasConTagsAsociados();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		long tinicio = System.currentTimeMillis();
		Queue<VOPeliculaPelicula> colaIn=new Queue<>();
		ILista<VOPeliculaPelicula> out=new ListaEncadenada<>();
		BufferedReader br=null;
		try{

			br =new BufferedReader(new FileReader(rutaRecomendacion));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion=line.split(",");
				int movieId=Integer.parseInt(separacion[0]);
				double rating = Double.parseDouble(separacion[1]);


				//-------Guarda en la cola la informaci�n
				VOPelicula peliculaActual=buscarPelicula(movieId);
				if(peliculaActual!=null && peliculaActual.getGenerosAsociados() != null){
					VOPeliculaPelicula peliculaRecom =new VOPeliculaPelicula();
					peliculaRecom.setPelicula(peliculaActual);
					colaIn.enqueue(peliculaRecom);
				}
				line=br.readLine();
			}
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		while(!colaIn.isEmpty()){
			VOPeliculaPelicula elem= procesarPeliculasRecomendadas(colaIn.dequeue());
			out.agregarElementoFinal(elem);
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return out;
	}




	public VOPeliculaPelicula procesarPeliculasRecomendadas(VOPeliculaPelicula pPelicula){
		long tinicio = System.currentTimeMillis();
		VOPelicula actual =pPelicula.getPelicula();
		if(actual!=null){
			VOGeneroPelicula generoActual=catalogoDeGeneros.buscarGenero(actual.getGenerosAsociados().darElemento(0));
			if(generoActual!=null){
				ILista<VOPelicula> listaPeliculasRecom=generoActual.getPeliculasPorPromedioRatings();
				if( listaPeliculasRecom!=null){

					Iterator<VOPelicula> iter=listaPeliculasRecom.iterator();
					while(iter.hasNext()){
						VOPelicula act=iter.next();
						if(act.getPromedioRatings()>= (actual.getPromedioRatings()-0.5) || act.getPromedioRatings()<=(actual.getPromedioRatings()+0.5)){
							ILista<VOPelicula> listaRecom=pPelicula.getPeliculasRelacionadas();
							act.setDiferenciaRats(Math.abs(act.getPromedioRatings()-actual.getPromedioRatings()));
							if(listaRecom!=null){
								listaRecom.agregarElementoFinal(act);
								pPelicula.setPeliculasRelacionadas(listaRecom);
							}else{
								ILista<VOPelicula> nueva=new ListaEncadenada<>();
								nueva.agregarElementoFinal(act);
								pPelicula.setPeliculasRelacionadas(nueva);
							}
						}
					}
					CompararPeliculas comp = new CompararPeliculas();
					comp.determinarCriterio(CompararPeliculas.DIFF_RATINGS);
					ShellSort.sort(comp, null, null, listaPeliculasRecom);	
				}

			}
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return pPelicula;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		long tinicio = System.currentTimeMillis();
		ILista<VORating> listaResp=new ListaEncadenada<>();
		VOPelicula resp=buscarPelicula((int)idPelicula);
		if(resp!=null){
			listaResp=resp.getRatingsAsociados();
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return listaResp;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		long tinicio = System.currentTimeMillis();
		ILista<VOGeneroUsuario> ret = catalogoDeGeneros.nUsuariosMasActivos(n);
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		long tinicio = System.currentTimeMillis();
		 ILista<VOUsuario> ret = catalogoDeUsuarios.getListaUsuarios();
		 long tfin = System.currentTimeMillis();
			agregarOperacionSR(methodName(), tinicio, tfin);
			return ret;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		long tinicio = System.currentTimeMillis();
		ILista<VOGeneroPelicula> ret = catalogoDeGeneros.peliculasConMayorNumeroDeTags(n);
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		long tinicio = System.currentTimeMillis();
		ILista<VOUsuarioGenero> ret = catalogoDeUsuarios.getListaUsuariosGenero();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		long tinicio = System.currentTimeMillis();
		Queue<VOPeliculaUsuario> colaIn=new Queue<>();
		ILista<VOPeliculaUsuario> out=new ListaEncadenada<>();
		
		BufferedReader br=null;
		Queue<Double> colaDouble=new Queue<>();
		try{

			br =new BufferedReader(new FileReader(rutaRecomendacion));
			String line=br.readLine();
			line=br.readLine();
			while(line!=null){
				String[] separacion=line.split(",");
				int movieId=Integer.parseInt(separacion[0]);
				double rating = Double.parseDouble(separacion[1]);
				colaDouble.enqueue(rating);

				//-------Guarda en la cola la informaci�n
				VOPelicula peliculaActual=buscarPelicula(movieId);
				if(peliculaActual!=null){
					VOPeliculaUsuario peliculaRecom =new VOPeliculaUsuario();
					peliculaRecom.setIdPelicula(movieId);
					colaIn.enqueue(peliculaRecom);
				}
				line=br.readLine();
			}
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		while(!colaIn.isEmpty()){
			VOPeliculaUsuario elem= procesarUsuariosRecomendados(colaIn.dequeue(),colaDouble.dequeue());
			out.agregarElementoFinal(elem);
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return out;
	}

	
	public VOPeliculaUsuario procesarUsuariosRecomendados(VOPeliculaUsuario pelUsuario,double pRating){
		
		VOPeliculaUsuario x=pelUsuario;
		VOPelicula actual=buscarPelicula((int)pelUsuario.getIdPelicula());
		if(actual!=null){
			ILista<VORating> listaRatings=actual.getRatingsAsociados();
			if(listaRatings!=null){
				Iterator<VORating> iter=listaRatings.iterator();
				while(iter.hasNext()){
					VORating act=iter.next();
					if(act.getRating()>=(pRating-0.5) && act.getRating()<=(pRating+0.5)){
						VOUsuario nuevo=catalogoDeUsuarios.buscarUsuario((int)act.getIdUsuario());
						nuevo.setDiferenciaOpinion(Math.abs(act.getRating()-pRating));
						ILista<VOUsuario> listaUsu=pelUsuario.getUsuariosRecomendados();
						if(listaUsu!=null){
							listaUsu.agregarElementoFinal(nuevo);
							pelUsuario.setUsuariosRecomendados(listaUsu);
						}else{
							listaUsu=new ListaEncadenada<>();
							listaUsu.agregarElementoFinal(nuevo);
							pelUsuario.setUsuariosRecomendados(listaUsu);
						}
					}
				}
			}
		}
		CompararUsuarios comp= new CompararUsuarios();
		comp.determinarCriterio(CompararUsuarios.DIFF_RATINGS);
		ShellSort.sort(comp, null, null, pelUsuario.getUsuariosRecomendados() );
		
		//ILista<VOUsuario> listaUsu=pelUsuario.getUsuariosRecomendados();
		
		return x;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		long tinicio = System.currentTimeMillis();
		ILista<VOTag> out= new ListaEncadenada<>();
		if(buscarPelicula(idPelicula)!=null){
			out=buscarPelicula(idPelicula).getTagsAsociadosVO();
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return out;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub
		VOOperacion oper = new VOOperacion();
		oper.setOperacion(nomOperacion);
		oper.setTimestampFin(tfin);
		oper.setTimestampInicio(tinicio);
		operaciones.push(oper);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		long tinicio = System.currentTimeMillis();
		ILista<VOOperacion> ret = new ListaEncadenada<>();
		while(!operaciones.isEmpty())
		{
			ret.agregarElementoFinal(operaciones.pop());
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		long tinicio = System.currentTimeMillis();
		// TODO Auto-generated method stub
		operaciones = null;
		operaciones = new Stack<VOOperacion>();
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		long tinicio = System.currentTimeMillis();
		ILista<VOOperacion> ret = new ListaEncadenada<>();
		IStack<VOOperacion> respaldo = new Stack<>();
		for(int i = 1; i <= n; i++){
			VOOperacion act = operaciones.pop();
			ret.agregarElementoFinal(act);
			respaldo.push(act);
		}
		while(!respaldo.isEmpty()){
			operaciones.push(respaldo.pop());
		}
		// TODO Auto-generated method stub
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return ret;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		long tinicio = System.currentTimeMillis();
		// TODO Auto-generated method stub
		for(int i = 1; i <= n; i++){
			operaciones.pop();
		}
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		long tinicio = System.currentTimeMillis();
		VOPelicula xAgregar = new VOPelicula();
		xAgregar.setAgnoPublicacion(agno);
		ILista<String> generosAsociados = new ListaEncadenada<>();
		for(String gen : generos){
			generosAsociados.agregarElementoFinal(gen);
		}
		xAgregar.setGenerosAsociados(generosAsociados);
		xAgregar.setTitulo(titulo);
		int id = misPeliculas.darNumeroElementos()+1;
		xAgregar.setId(id);
		misPeliculas.agregarElementoFinal(xAgregar);
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);
		return id;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		long tinicio = System.currentTimeMillis();
		// TODO Auto-generated method stub
		ILista<VORating> ratings = buscarPelicula(idPelicula).getRatingsAsociados();
		VORating xAgregar = new VORating();
		xAgregar.setIdPelicula((long)idPelicula);
		xAgregar.setIdUsuario((long)idUsuario);
		xAgregar.setRating(rating);
		xAgregar.setTimeStamp(System.currentTimeMillis());
		ratings.agregarElementoFinal(xAgregar);
		buscarPelicula(idPelicula).setRatingsAsociados(ratings);
		int numRats = catalogoDeUsuarios.buscarUsuario(idUsuario).getNumRatings();
		catalogoDeUsuarios.buscarUsuario(idUsuario).setNumRatings(numRats);
		long tfin = System.currentTimeMillis();
		agregarOperacionSR(methodName(), tinicio, tfin);

	}

	public VOPelicula buscarPelicula(int pIdPelicula){

		VOPelicula x=null;
		if(misPeliculas!=null){
			boolean esc=false;
			Iterator<VOPelicula> iter= misPeliculas.iterator();
			while(iter.hasNext() && !esc){
				VOPelicula act=iter.next();
				if(act.getId()==pIdPelicula){
					x=act;
				}
			}
		}
		return x;
	}
	
	public String methodName(){
		return new Exception().getStackTrace()[1].getMethodName();
	}

}
