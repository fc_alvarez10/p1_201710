package model.logic;

import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.sort.CompararPeliculas;
import model.sort.CompararUsuarioConteo;
import model.sort.CompararUsuarios;
import model.sort.ShellSort;
import model.vo.*;

public class CatalogoGeneros {

	private ILista<VOGeneroPelicula> listaGeneros; // Se ordena por ratings
	
	private ILista<VOGeneroUsuario> listaGeneroUsuarios;
	

	public ILista<VOGeneroUsuario> getListaUsuarios() {
		return listaGeneroUsuarios;
	}

	public void setListaUsuarios(ILista<VOGeneroUsuario> listaUsuarios) {
		this.listaGeneroUsuarios = listaUsuarios;
	}

	public ILista<VOGeneroPelicula> getListaGeneros(){
		return listaGeneros;
	}

	public void setListaGeneros(ILista<VOGeneroPelicula> pListaGeneros){
		listaGeneros=pListaGeneros;
	}

	public boolean generoExiste(String pGenero){

		boolean x=false;
		boolean esc=false;
		if(listaGeneros!=null){
			Iterator<VOGeneroPelicula> iter=listaGeneros.iterator();
			while(iter.hasNext() && !esc){

				VOGeneroPelicula act=iter.next();
				if(act.getGenero().equals(pGenero)){
					x=true;
					esc=true;
				}

			}
		}
		return x;
	}

	public VOGeneroPelicula buscarGenero(String pGenero){
		VOGeneroPelicula x=null;
		boolean esc=false;
		if(listaGeneros!=null){
			Iterator<VOGeneroPelicula> iter=listaGeneros.iterator();
			while(iter.hasNext() && !esc){

				VOGeneroPelicula act=iter.next();
				if(act.getGenero().equals(pGenero)){
					x=act;
					esc=true;
				}

			}
		}
		return x;
	}
	
	public VOGeneroUsuario buscarGeneroUsuario(String pGenero){
		VOGeneroUsuario x=null;
		boolean esc=false;
		if(listaGeneroUsuarios!=null){
			Iterator<VOGeneroUsuario> iter=listaGeneroUsuarios.iterator();
			while(iter.hasNext() && !esc){

				VOGeneroUsuario act=iter.next();
				if(act.getGenero().equals(pGenero)){
					x=act;
					esc=true;
				}

			}
		}
		return x;
	}

	public void agregarNuevoGenero(VOGeneroPelicula pGenero){
		if(listaGeneros!=null){
			listaGeneros.agregarElementoFinal(pGenero);
		}else{
			ILista<VOGeneroPelicula> nuevaLista=new ListaEncadenada<>();
			nuevaLista.agregarElementoFinal(pGenero);
			this.setListaGeneros(nuevaLista);
		}
	}

	public ILista<VOGeneroPelicula> nPeliculasMasPopulares(int n){

		ILista<VOGeneroPelicula> out=new ListaEncadenada<>();
		if(listaGeneros!=null){

			Iterator<VOGeneroPelicula> iter =listaGeneros.iterator();
			while(iter.hasNext()){
				int cont=0;
				VOGeneroPelicula generoAct=iter.next();
				VOGeneroPelicula nuevoGenero=new VOGeneroPelicula();
				nuevoGenero.setGenero(generoAct.getGenero());
				ILista<VOPelicula> listaPeliculas=new ListaEncadenada<>();
				Iterator<VOPelicula> iter2=generoAct.getPeliculasPorPopularidad().iterator();
				while(iter2.hasNext() && cont<=n){
					VOPelicula peliculaAct=iter2.next();
					listaPeliculas.agregarElementoFinal(peliculaAct);
					cont++;
				}
				nuevoGenero.setPeliculas(listaPeliculas);
				out.agregarElementoFinal(nuevoGenero);
			}
		}
		return out;

	}
	
	public ILista<VOGeneroPelicula> peliculasMejorCalificacionPromedio(){
		ILista<VOGeneroPelicula> out=new ListaEncadenada<>();
		
		if(listaGeneros!=null){
			Iterator<VOGeneroPelicula> iter=listaGeneros.iterator();
			while(iter.hasNext()){
				VOGeneroPelicula act=iter.next();
				ILista<VOPelicula> x=act.getPeliculasPorPromedioRatings();
				ILista<VOPelicula> y=new ListaEncadenada<>();
				y.agregarElementoFinal(x.darElemento(0));
				VOGeneroPelicula nuevo=new VOGeneroPelicula();
				nuevo.setGenero(act.getGenero());
				nuevo.setPeliculas(y);
				out.agregarElementoFinal(nuevo);
			}
		}
		return out;
	}
	
	public ILista<VOGeneroPelicula> peliculasConTagsAsociados(){
		
		ILista<VOGeneroPelicula> out= new ListaEncadenada<>();
		
		Iterator<VOGeneroPelicula> iter = listaGeneros.iterator();
		while(iter.hasNext()){
			VOGeneroPelicula genAct=iter.next();
			VOGeneroPelicula genero=new VOGeneroPelicula();
			ILista<VOPelicula> listaPeli=new ListaEncadenada<>();
			Iterator<VOPelicula> iter2=genAct.getPeliculasPorFecha().iterator();
			while(iter2.hasNext()){
				VOPelicula peliAct=iter2.next();
				listaPeli.agregarElementoFinal(peliAct);
			}
			genero.setGenero(genAct.getGenero());
			genero.setPeliculas(listaPeli);
			out.agregarElementoFinal(genero);
		}
		return out;
		
	}
	
	public ILista<VOGeneroUsuario> nUsuariosMasActivos(int n){
		
		ILista<VOGeneroUsuario> out = new ListaEncadenada<>();
		Iterator<VOGeneroUsuario> iter= listaGeneroUsuarios.iterator();
		while(iter.hasNext()){
			int cont=0;
			VOGeneroUsuario act=iter.next();
			VOGeneroUsuario nuevo=new VOGeneroUsuario();
			nuevo.setGenero(act.getGenero());
			ILista<VOUsuarioConteo> listaUsuCont=new ListaEncadenada<>();
			Iterator<VOUsuarioConteo> iter2=act.getUsuarios().iterator();
			while(iter2.hasNext() && cont<=n){
				VOUsuarioConteo nuevoUsu=iter2.next();
				listaUsuCont.agregarElementoFinal(nuevoUsu);
				cont++;
			}
			nuevo.setUsuarios(listaUsuCont);
			out.agregarElementoFinal(nuevo);
		}
		return out;
	}
	
	public ILista<VOGeneroPelicula> peliculasConMayorNumeroDeTags(int n){
		ILista<VOGeneroPelicula> out=new ListaEncadenada<>();
		
		Iterator<VOGeneroPelicula> iter=listaGeneros.iterator();
		while(iter.hasNext()){
			VOGeneroPelicula act=iter.next();
			VOGeneroPelicula nuevoGen=new VOGeneroPelicula();
			nuevoGen.setGenero(act.getGenero());
			int cont=0;
			ILista<VOPelicula> listaPel=new ListaEncadenada<>();
			Iterator<VOPelicula> iter2=act.getPeliculasPorNumeroTags().iterator();
			while(iter2.hasNext() && cont<=n){
				VOPelicula peliAct=iter2.next();
				listaPel.agregarElementoFinal(peliAct);
				cont++;
			}
			nuevoGen.setPeliculas(listaPel);
			out.agregarElementoFinal(nuevoGen);
		}
		
		return out;
		
	}
	
	public void ordenarListas(){
		CompararPeliculas comp = new CompararPeliculas();
		comp.determinarCriterio(CompararPeliculas.NUM_RATINGS);
		CompararPeliculas comp1 = new CompararPeliculas();
		comp1.determinarCriterio(CompararPeliculas.MEAN_RATINGS);
		CompararPeliculas comp3 = new CompararPeliculas();
		comp3.determinarCriterio(CompararPeliculas.NUM_TAGS);
		Iterator<VOGeneroPelicula> iter = listaGeneros.iterator();
		while (iter.hasNext()){
			VOGeneroPelicula act = iter.next();
			ShellSort.sort(comp, null, null, act.getPeliculas());
			act.setPeliculasPorPopularidad(act.getPeliculas());
			ShellSort.sort(comp1, null, null, act.getPeliculas());
			act.setPeliculasPorPromedioRatings(act.getPeliculas());
			ShellSort.sort(comp3, null, null, act.getPeliculas());
			act.setPeliculasPorNumeroTags(act.getPeliculas());
		}
		CompararUsuarioConteo comp2 = new CompararUsuarioConteo();
		Iterator<VOGeneroUsuario> iter2 = listaGeneroUsuarios.iterator();
		while (iter2.hasNext()){
			VOGeneroUsuario act = iter2.next();
			ShellSort.sort(comp2, null, null, act.getUsuarios());
		}
		
	}
}
