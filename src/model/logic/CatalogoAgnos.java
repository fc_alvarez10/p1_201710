package model.logic;

import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.sort.CompararAgnos;
import model.sort.CompararPeliculas;
import model.sort.ShellSort;
import model.vo.VOAgnoPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;


public class CatalogoAgnos {

	private ILista<VOAgnoPelicula> peliculasPorAgno;
	
	public ILista<VOAgnoPelicula> getPeliculasPorAgno() {
		return peliculasPorAgno;
	}
	
	public void setPeliculasPorAgno(ILista<VOAgnoPelicula> peliculasPorAgno) {
		this.peliculasPorAgno = peliculasPorAgno;
	}
	
	public boolean agnoExiste(int pAgno){
		boolean x=false;
		if(peliculasPorAgno!=null){
			boolean esc=false;
			Iterator<VOAgnoPelicula> iter=peliculasPorAgno.iterator();
			while(!esc && iter.hasNext()){
				VOAgnoPelicula act =iter.next();
				if(act.getAgno()==pAgno){
					x=true;
					esc=true;
				}
					
			}
		}
		return x;
	}
	
	public VOAgnoPelicula buscarAgno(int pAgno){
		VOAgnoPelicula x=null;
		boolean esc=false;
		if(peliculasPorAgno!=null){
			Iterator<VOAgnoPelicula> iter=peliculasPorAgno.iterator();
			while(iter.hasNext() && !esc){
				
				VOAgnoPelicula act=iter.next();
				if(act.getAgno()==pAgno){
					x=act;
					esc=true;
				}
				
			}
		}
		return x;
	}
	
	public void agregarNuevoAgno(VOAgnoPelicula pAgno){
		if(peliculasPorAgno!=null){
		peliculasPorAgno.agregarElementoFinal(pAgno);
		}else{
			ILista<VOAgnoPelicula> nuevaLista=new ListaEncadenada<>();
			nuevaLista.agregarElementoFinal(pAgno);
			this.setPeliculasPorAgno(nuevaLista);
		}
	}
	
	public ILista<VOPelicula> peliculasConcatenadas(){
		
		ILista<VOPelicula> out=new ListaEncadenada<>();
		Iterator<VOAgnoPelicula> iter=peliculasPorAgno.iterator();
		while(iter.hasNext()){
			VOAgnoPelicula agnoAct= iter.next();
			Iterator<VOPelicula> iter2= agnoAct.getPeliculas().iterator();
			while(iter2.hasNext()){
				VOPelicula peliculaAct=iter2.next();
				out.agregarElementoFinal(peliculaAct);
			}
		}
		return out;
	}
	
	public void ordenarPeliculas(){
		CompararAgnos comp = new CompararAgnos();
		ShellSort.sort(comp, null, null, peliculasPorAgno);
		CompararPeliculas comp1 = new CompararPeliculas();
		comp1.determinarCriterio(CompararPeliculas.MEAN_RATINGS);
		CompararPeliculas comp2 = new CompararPeliculas();
		comp2.determinarCriterio(CompararPeliculas.TITULO);
		Iterator<VOAgnoPelicula> iter=peliculasPorAgno.iterator();
		while(iter.hasNext()){
			VOAgnoPelicula act=iter.next();
			ShellSort.sort(comp1, comp2, null, act.getPeliculas());
		}
	}
	
}
