package model.logic;

import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.sort.CompararTagString;
import model.sort.CompararTags;
import model.sort.CompararUsuarios;
import model.sort.ShellSort;
import model.vo.VOGeneroTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

public class CatalogoUsuarios {

	private ILista<VOUsuario> listaUsuarios;
	
	private ILista<VOUsuarioGenero> listaUsuariosGenero;
	
	public ILista<VOUsuarioGenero> getListaUsuariosGenero() {
		return listaUsuariosGenero;
	}

	public void setListaUsuariosGenero(ILista<VOUsuarioGenero> listaUsuariosGenero) {
		this.listaUsuariosGenero = listaUsuariosGenero;
	}

	public ILista<VOUsuario> getListaUsuarios(){
		return this.listaUsuarios;
	}
	
	public void setListaUsuarios(ILista<VOUsuario> pListaUsuarios){
		listaUsuarios=pListaUsuarios;
	}
	
	public boolean existeUsuario(int pIdUsuario){
		
		boolean x=false;
		boolean esc=false;
		if(listaUsuarios!=null){
			Iterator<VOUsuario> iter=listaUsuarios.iterator();
			while(iter.hasNext() && !esc){
				VOUsuario act=iter.next();
				if(act.getIdUsuario()==pIdUsuario){
					x=true;
					esc=true;
				}
			}
		}
		return x;
		
	}
	
	public VOUsuario buscarUsuario(int pIdUsuario){
		VOUsuario x=null;
		boolean esc=false;
		if(listaUsuarios!=null){
			Iterator<VOUsuario> iter=listaUsuarios.iterator();
			while(iter.hasNext() && !esc){
				VOUsuario act=iter.next();
				if(act.getIdUsuario()==pIdUsuario){
					x=act;
					esc=true;
				}
			}
		}
		return x;
		
	}
	
	public boolean existeUsuarioGenero(int pIdUsuario){
		
		boolean x=false;
		boolean esc=false;
		if(listaUsuariosGenero!=null){
			Iterator<VOUsuarioGenero> iter=listaUsuariosGenero.iterator();
			while(iter.hasNext() && !esc){
				VOUsuarioGenero act=iter.next();
				if(act.getIdUsuario()==pIdUsuario){
					x=true;
					esc=true;
				}
			}
		}
		return x;
		
	}

	public VOUsuarioGenero buscarUsuarioGenero(int pIdUsuario){
		VOUsuarioGenero x=null;
		boolean esc=false;
		if(listaUsuariosGenero!=null){
			Iterator<VOUsuarioGenero> iter=listaUsuariosGenero.iterator();
			while(iter.hasNext() && !esc){
				VOUsuarioGenero act=iter.next();
				if(act.getIdUsuario()==pIdUsuario){
					x=act;
					esc=true;
				}
			}
		}
		return x;
		
	}
	
	public VOGeneroTag buscarGeneroTag(VOUsuarioGenero pUsuarioGenero, String pGenero){
		
		VOGeneroTag out=null;
		if(pUsuarioGenero.getListaGeneroTags()!=null){
			boolean esc=false;
			Iterator<VOGeneroTag> iter=pUsuarioGenero.getListaGeneroTags().iterator();
			while(iter.hasNext() && !esc){
				VOGeneroTag act=iter.next();
				if(act.getGenero().equals(pGenero)){
					out=act;
					esc=true;
				}
			}
		}
		
		return out;
		
	}

	public void agregarUsuarioNuevo(VOUsuario nuevoUsuario){
		if(listaUsuarios!=null){
			listaUsuarios.agregarElementoFinal(nuevoUsuario);
		}else{
			ILista<VOUsuario> nuevaLista=new ListaEncadenada<>();
			nuevaLista.agregarElementoFinal(nuevoUsuario);
			setListaUsuarios(nuevaLista);
		}
	}
	
	public void agregarUsuarioGeneroNuevo(VOUsuarioGenero nuevoUsuarioGenero){
		if(listaUsuariosGenero!=null){
			listaUsuariosGenero.agregarElementoFinal(nuevoUsuarioGenero);
		}else{
			ILista<VOUsuarioGenero> nuevaLista=new ListaEncadenada<>();
			nuevaLista.agregarElementoFinal(nuevoUsuarioGenero);
			setListaUsuariosGenero(nuevaLista);
		}
	}
	
	public void ordenarUsuarios(){
		CompararUsuarios comp1 = new CompararUsuarios();
		comp1.determinarCriterio(CompararUsuarios.TIME_FIRST_RAT);
		CompararUsuarios comp2 = new CompararUsuarios();
		comp2.determinarCriterio(CompararUsuarios.NUM_RATINGS);
		CompararUsuarios comp3 = new CompararUsuarios();
		comp3.determinarCriterio(CompararUsuarios.ID);
		ShellSort.sort(comp1, comp2, comp3, listaUsuarios);
		CompararTagString comp4 = new CompararTagString();
		Iterator<VOUsuarioGenero> iter=listaUsuariosGenero.iterator();
		while(iter.hasNext()){
			VOUsuarioGenero act=iter.next();
			Iterator<VOGeneroTag> iter2=act.getListaGeneroTags().iterator();
			while(iter2.hasNext()){
				VOGeneroTag act2 = iter2.next();
			ShellSort.sort(comp4, null, null, act2.getTags());
			}
		}
	}
	
	
}
